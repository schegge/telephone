package de.schegge.phone;

import de.schegge.phone.PhoneNumberFormatterBuilder.PhoneNumberPart;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

public final class PhoneNumberFormatter {

    public static final PhoneNumberFormatter DIN_5008 = new PhoneNumberFormatterBuilder().toFormatter("annn sss[-eee]");
    public static final PhoneNumberFormatter E_123 = new PhoneNumberFormatterBuilder().toFormatter("(annn) sss[-eee]");
    public static final PhoneNumberFormatter INTERNATIONAL_DIN_5008 = new PhoneNumberFormatterBuilder().toFormatter("+ccc nnn sss[-eee]");
    public static final PhoneNumberFormatter INTERNATIONAL_E_123 = new PhoneNumberFormatterBuilder().toFormatter("+ccc (nnn) sss[-eee]");
    public static final PhoneNumberFormatter PREFIXED_DIN_5008 = new PhoneNumberFormatterBuilder().toFormatter("IIIccc nnn sss[-eee]");
    public static final PhoneNumberFormatter PREFIXED_E_123 = new PhoneNumberFormatterBuilder().toFormatter("IIIccc (nnn) sss[-eee]");

    private final List<PhoneNumberPart> partList;
    private final Locale locale;
    private final String internationalDialingPrefix;
    private final String nationalAccessCode;

    PhoneNumberFormatter(List<PhoneNumberPart> parts, Locale locale, String internationalDialingPrefix, String nationalAccessCode) {
        this.partList = parts;
        this.locale = locale;
        this.internationalDialingPrefix = internationalDialingPrefix;
        this.nationalAccessCode = nationalAccessCode;
    }

    public static PhoneNumberFormatter ofPattern(String pattern) {
        return ofPattern(pattern, Locale.getDefault());
    }

    public static PhoneNumberFormatter ofPattern(String pattern, Locale locale) {
        return new PhoneNumberFormatterBuilder().toFormatter(pattern, locale);
    }

    public InternationalPhoneNumber parse(String text) {
        PhoneNumberFormatterContext context = new PhoneNumberFormatterContext(this);
        int pos = 0;
        for (PhoneNumberPart part : partList) {
            pos = part.parse(context, text, pos);
            if (pos < 0) {
                throw new IllegalArgumentException("parse exception at position " + -pos + ": " + text);
            }
        }
        return getInternationalPhoneNumber(context.parts);
    }

    InternationalPhoneNumber getInternationalPhoneNumber(CharSequence[] parts) {
        String currentNationalAccessCode = Objects.toString(parts[2], this.nationalAccessCode);
        String currentNationalDestinationCode = String.valueOf(parts[3]);
        String currentSubscriberNumber = String.valueOf(parts[4]);
        String currentExtension = Objects.toString(parts[5], null);
        NationalPhoneNumber nationalPhoneNumber;
        if (currentExtension != null) {
            nationalPhoneNumber = NationalPhoneNumber.of(currentNationalAccessCode, currentNationalDestinationCode, currentSubscriberNumber, currentExtension);
        } else {
            nationalPhoneNumber = NationalPhoneNumber.of(currentNationalAccessCode, currentNationalDestinationCode, currentSubscriberNumber);
        }
        String currentInternationalDialingPrefix = Objects.toString(parts[0], this.internationalDialingPrefix);
        return InternationalPhoneNumber.of(currentInternationalDialingPrefix, Objects.toString(parts[1]), nationalPhoneNumber);
    }

    public String format(PhoneNumber phoneNumber) {
        PhoneNumberFormatterContext context = new PhoneNumberFormatterContext(this, getParts(phoneNumber));
        StringBuilder builder = new StringBuilder();
        for (PhoneNumberPart part : partList) {
            part.format(context, builder);
        }
        return builder.toString();
    }

    private String[] getParts(InternationalPhoneNumber internationalPhoneNumber) {
        return new String[]{
                internationalPhoneNumber.getInternationalDialingPrefix(),
                internationalPhoneNumber.getCountryCode(),
                internationalPhoneNumber.getNationalAccessCode().orElse(null),
                internationalPhoneNumber.getNationalDestinationCode(),
                internationalPhoneNumber.getSubscriberNumber(),
                internationalPhoneNumber.getExtension().orElse(null),
                null,
                null
        };
    }

    private String[] getParts(NationalPhoneNumber nationalPhoneNumber) {
        return new String[]{
                null,
                null,
                nationalPhoneNumber.getNationalAccessCode().orElse(null),
                nationalPhoneNumber.getNationalDestinationCode(),
                nationalPhoneNumber.getSubscriberNumber(),
                nationalPhoneNumber.getExtension().orElse(null),
                null,
                null
        };
    }

    private String[] getParts(PhoneNumber phoneNumber) {
        if (phoneNumber instanceof InternationalPhoneNumber internationalPhoneNumber) {
            return getParts(internationalPhoneNumber);
        }
        if (phoneNumber instanceof NationalPhoneNumber nationalPhoneNumber) {
            return getParts(nationalPhoneNumber);
        }
        throw new IllegalArgumentException("type yet not supported");
    }

    public Locale getLocale() {
        return locale;
    }

    List<PhoneNumberPart> getPartList() {
        return partList;
    }

    public PhoneNumberFormatter withLocale(Locale locale) {
        return this.locale.equals(Objects.requireNonNull(locale)) ? this : new PhoneNumberFormatter(partList, locale, internationalDialingPrefix, nationalAccessCode);
    }
}