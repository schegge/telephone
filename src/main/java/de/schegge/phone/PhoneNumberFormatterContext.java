package de.schegge.phone;

class PhoneNumberFormatterContext {

  enum Parts {
    INTERNATIONAL_DIALING_PREFIX,
    COUNTRY_CALLING_CODE,
    NATIONAL_ACCESS_CODE,
    NATIONAL_DESTINATION_CODE,
    SUBSCRIBER_NUMBER,
    EXTENSION,
    BLOCK_START,
    BLOCK_END,
    BLOCK_PATTERN
  }

  final PhoneNumberFormatter formatter;
  final CharSequence[] parts;

  PhoneNumberFormatterContext(PhoneNumberFormatter formatter) {
    this.formatter = formatter;
    this.parts = new CharSequence[Parts.values().length];
  }

  PhoneNumberFormatterContext(PhoneNumberFormatter formatter, CharSequence[] parts) {
    this.formatter = formatter;
    this.parts = parts;
  }
}
