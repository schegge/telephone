package de.schegge.phone;

import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

public final class NationalPhoneNumber implements PhoneNumber {

    private final String nationalAccessCode;
    private final String nationalDestinationCode;
    private final String subscriberNumber;
    private final String extension;

    private NationalPhoneNumber(String nationalAccessCode, String nationalDestinationCode, String subscriberNumber, String extension) {
        this.nationalAccessCode = requireNonNull(nationalAccessCode);
        this.nationalDestinationCode = requireNonNull(nationalDestinationCode);
        this.subscriberNumber = requireNonNull(subscriberNumber);
        this.extension = extension;
    }

    private NationalPhoneNumber with(String nationalAccessCode) {
        return new NationalPhoneNumber(nationalAccessCode, nationalDestinationCode, subscriberNumber,extension);
    }

    public static NationalPhoneNumber of(NationalPhoneNumber nationalPhoneNumber, String subscriberNumber, String extension) {
        return new NationalPhoneNumber(nationalPhoneNumber.nationalAccessCode, nationalPhoneNumber.nationalDestinationCode, subscriberNumber, extension);
    }

    public static NationalPhoneNumber of(String nationalAccessCode, String nationalDestinationCode, String subscriberNumber) {
        return new NationalPhoneNumber(nationalAccessCode, nationalDestinationCode, subscriberNumber, null);
    }

    public static NationalPhoneNumber of(String nationalAccessCode, String nationalDestinationCode, String subscriberNumber, String extension) {
        return new NationalPhoneNumber(nationalAccessCode, nationalDestinationCode, subscriberNumber, extension);
    }

    public static NationalPhoneNumber from(PhoneNumber phoneNumber) {
        return new NationalPhoneNumber(phoneNumber.getNationalAccessCode()
                .orElseThrow(() -> new IllegalArgumentException("missing national access code")),
                phoneNumber.getNationalDestinationCode(), phoneNumber.getSubscriberNumber(),
                phoneNumber.getExtension().orElse(null));
    }

    public static NationalPhoneNumber parse(String text) {
        return parse(text, PhoneNumberFormatter.DIN_5008);
    }

    public static NationalPhoneNumber parse(String text, PhoneNumberFormatter formatter) {
        return formatter.parse(text).toNational();
    }

    @Override
    public Optional<String> getNationalAccessCode() {
        return Optional.ofNullable(nationalAccessCode);
    }

    @Override
    public String getNationalDestinationCode() {
        return nationalDestinationCode;
    }

    @Override
    public String getSubscriberNumber() {
        return subscriberNumber;
    }

    @Override
    public Optional<String> getExtension() {
        return Optional.ofNullable(extension);
    }

    public InternationalPhoneNumber toInternational(Locale country) {
        Localization localization = Localization.getLocalizations(country);
        String internationalDialingPrefix = localization.internationalDialingPrefix();
        String countryCallingCode = localization.countryCallingCode();
        return InternationalPhoneNumber.of(internationalDialingPrefix, countryCallingCode, this.with(localization.nationalAccessCode()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NationalPhoneNumber that = (NationalPhoneNumber) o;
        return Objects.equals(nationalAccessCode, that.nationalAccessCode) && nationalDestinationCode
                .equals(that.nationalDestinationCode) &&
                subscriberNumber.equals(that.subscriberNumber) && Objects.equals(extension, that.extension);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nationalAccessCode, nationalDestinationCode, subscriberNumber, extension);
    }

    @Override
    public String toString() {
        return PhoneNumberFormatter.DIN_5008.format(this);
    }

    @Override
    public boolean matches(PhoneNumber number) {
        return equals(number);
    }

    @Override
    public boolean hasCountryCode() {
        return false;
    }
}
