package de.schegge.phone;

import java.util.Objects;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class PhoneNumberBlock implements PhoneNumberAccessor {
    private static final String INVALID_BLOCK = "invalid block: ";

    private final InternationalPhoneNumber phoneNumber;
    private final String blockStart;
    private final String blockEnd;

    private PhoneNumberBlock(InternationalPhoneNumber phoneNumber, String blockStart, String blockEnd) {
        this.phoneNumber = Objects.requireNonNull(phoneNumber);
        this.blockStart = Objects.requireNonNull(blockStart);
        this.blockEnd = Objects.requireNonNull(blockEnd);
    }

    public InternationalPhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public String getBlockStart() {
        return blockStart;
    }

    public String getBlockEnd() {
        return blockEnd;
    }

    public int getBlockSize() {
        int start = Integer.parseInt(blockStart);
        int end = Integer.parseInt(blockEnd);
        return end - start + 1;
    }

    public Stream<InternationalPhoneNumber> phoneNumbers() {
        int start = Integer.parseInt(blockStart);
        int end = Integer.parseInt(blockEnd);
        return IntStream.rangeClosed(start, end).mapToObj(String::valueOf).map(x -> "0".repeat(blockStart.length() - x.length()) + x).map(this::createPhoneNumber);
    }

    public InternationalPhoneNumber getFirst() {
        return createPhoneNumber(getBlockStart());
    }

    public InternationalPhoneNumber getLast() {
        return createPhoneNumber(getBlockEnd());
    }

    public boolean matches(PhoneNumber phoneNumber) {
        if (phoneNumber == null || !phoneNumber.hasCountryCode()) {
            return false;
        }
        return phoneNumber.getCountryCode().equals(this.phoneNumber.getCountryCode()) &&
                phoneNumber.getNationalDestinationCode().equals(this.phoneNumber.getNationalDestinationCode()) &&
                contains(phoneNumber.getSubscriberNumber());
    }

    private boolean contains(String subscriberNumber) {
        if (!subscriberNumber.startsWith(getPhoneNumber().getSubscriberNumber())) {
            return false;
        }
        int start = Integer.parseInt(blockStart);
        int end = Integer.parseInt(blockEnd);
        int between = Integer.parseInt(subscriberNumber.substring(getPhoneNumber().getSubscriberNumber().length()));
        return start <= between && between <= end;
    }

    private InternationalPhoneNumber createPhoneNumber(String suffix) {
        NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.of(phoneNumber.toNational(), phoneNumber.getSubscriberNumber() + suffix, null);
        return InternationalPhoneNumber.of(phoneNumber, nationalPhoneNumber);
    }

    public String format(PhoneNumberBlockFormatter formatter) {
        return formatter.format(this);
    }

    public static PhoneNumberBlock parse(String text) {
        return PhoneNumberBlockFormatter.INTERNATIONAL_BLOCK.parse(text);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PhoneNumberBlock that = (PhoneNumberBlock) o;
        return Objects.equals(blockStart, that.blockStart) && Objects.equals(blockEnd, that.blockEnd) && Objects.equals(phoneNumber, that.phoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(blockStart, blockEnd, phoneNumber);
    }

    @Override
    public String toString() {
        return PhoneNumberBlockFormatter.INTERNATIONAL_BLOCK.format(this);
    }

    public static PhoneNumberBlock of(InternationalPhoneNumber phoneNumber, String blockStart, String blockEnd) {
        if (Objects.requireNonNull(blockStart).length() != Objects.requireNonNull(blockEnd).length()) {
            throw new IllegalArgumentException(INVALID_BLOCK + blockStart + "-" + blockEnd);
        }
        try {
            int start = Integer.parseInt(blockStart);
            int end = Integer.parseInt(blockEnd);
            if (start < 0 || start >= end) {
                throw new IllegalArgumentException(INVALID_BLOCK + blockStart + "-" + blockEnd);
            }
            return new PhoneNumberBlock(phoneNumber, blockStart, blockEnd);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(INVALID_BLOCK + blockStart + "-" + blockEnd, e);
        }
    }
}
