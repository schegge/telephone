package de.schegge.phone.freshmarker;

import de.schegge.phone.InternationalPhoneNumber;
import de.schegge.phone.NationalPhoneNumber;
import de.schegge.phone.PhoneNumber;
import de.schegge.phone.PhoneNumberBlock;
import org.freshmarker.core.buildin.BuiltIn;
import org.freshmarker.core.buildin.BuiltInKey;
import org.freshmarker.core.buildin.BuiltInKeyBuilder;
import org.freshmarker.core.model.TemplateListSequence;
import org.freshmarker.core.model.TemplateObject;
import org.freshmarker.core.model.primitive.TemplateString;
import org.freshmarker.core.plugin.PluginProvider;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class PhoneNumberPluginProvider implements PluginProvider {
    private static final BuiltInKeyBuilder<TemplatePhoneNumber> PHONE_NUMBER_BUILDER = new BuiltInKeyBuilder<>(TemplatePhoneNumber.class);
    private static final BuiltInKeyBuilder<TemplatePhoneNumberBlock> PHONE_NUMBER_BLOCK_BUILDER = new BuiltInKeyBuilder<>(TemplatePhoneNumberBlock.class);

    @Override
    public void registerMapper(Map<Class<?>, Function<Object, TemplateObject>> mapper) {
        mapper.put(InternationalPhoneNumber.class, o -> new TemplatePhoneNumber((PhoneNumber) o, true));
        mapper.put(NationalPhoneNumber.class, o -> new TemplatePhoneNumber((PhoneNumber) o, false));
        mapper.put(PhoneNumberBlock.class, o -> new TemplatePhoneNumberBlock((PhoneNumberBlock) o));
    }

    @Override
    public void registerBuildIn(Map<BuiltInKey, BuiltIn> builtIns) {
        builtIns.put(PHONE_NUMBER_BUILDER.of("national"), (x, y, e) -> new TemplatePhoneNumber(((TemplatePhoneNumber) x).getNational(), false));
        builtIns.put(PHONE_NUMBER_BUILDER.of("ndc"), (x, y, e) -> new TemplateString(((TemplatePhoneNumber) x).getValue().getNationalDestinationCode()));
        builtIns.put(PHONE_NUMBER_BUILDER.of("sn"), (x, y, e) -> new TemplateString(((TemplatePhoneNumber) x).getValue().getSubscriberNumber()));
        builtIns.put(PHONE_NUMBER_BUILDER.of("ext"), (x, y, e) -> new TemplateString(((TemplatePhoneNumber) x).getValue().getExtension().orElse("")));
        builtIns.put(PHONE_NUMBER_BLOCK_BUILDER.of("first"), (x, y, e) -> ((TemplatePhoneNumberBlock) x).getFirst());
        builtIns.put(PHONE_NUMBER_BLOCK_BUILDER.of("last"), (x, y, e) -> ((TemplatePhoneNumberBlock) x).getLast());
        builtIns.put(PHONE_NUMBER_BLOCK_BUILDER.of("list"), (x, y, e) -> getSequence((TemplatePhoneNumberBlock) x));
    }

    private static TemplateObject getSequence(TemplatePhoneNumberBlock x) {
        List<Object> templatePhoneNumbers = x.getValue().phoneNumbers().map(y -> new TemplatePhoneNumber(y, true)).map(Object.class::cast).toList();
        return new TemplateListSequence(templatePhoneNumbers);
    }
}
