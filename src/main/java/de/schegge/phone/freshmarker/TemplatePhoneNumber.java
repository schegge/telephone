package de.schegge.phone.freshmarker;

import de.schegge.phone.InternationalPhoneNumber;
import de.schegge.phone.PhoneNumber;
import org.freshmarker.core.model.primitive.TemplatePrimitive;

public class TemplatePhoneNumber extends TemplatePrimitive<PhoneNumber> {
    private final boolean international;
    public TemplatePhoneNumber(PhoneNumber phoneNumber, boolean international) {
        super(phoneNumber);
        this.international = international;
    }

    public PhoneNumber getNational() {
        return international ? ((InternationalPhoneNumber)getValue()).toNational() : getValue();
    }
}
