package de.schegge.phone.freshmarker;

import de.schegge.phone.PhoneNumberBlock;
import org.freshmarker.core.model.TemplateObject;
import org.freshmarker.core.model.primitive.TemplatePrimitive;

public class TemplatePhoneNumberBlock extends TemplatePrimitive<PhoneNumberBlock> {
    public TemplatePhoneNumberBlock(PhoneNumberBlock phoneNumber) {
        super(phoneNumber);
    }

    public TemplateObject getFirst() {
        return new TemplatePhoneNumber(getValue().getFirst(), true);
    }

    public TemplateObject getLast() {
        return new TemplatePhoneNumber(getValue().getLast(), true);
    }
}
