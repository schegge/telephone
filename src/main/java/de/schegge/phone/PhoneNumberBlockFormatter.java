package de.schegge.phone;

import java.util.Locale;
import java.util.Objects;

public class PhoneNumberBlockFormatter {
    public static final PhoneNumberBlockFormatter NATIONAL_BLOCK = new PhoneNumberBlockFormatter(new PhoneNumberFormatterBuilder().toFormatter("annn sss zzz"));
    public static final PhoneNumberBlockFormatter INTERNATIONAL_BLOCK = new PhoneNumberBlockFormatter(new PhoneNumberFormatterBuilder().toFormatter("+ccc nnn sss zzz"));

    private final PhoneNumberFormatter formatter;

    PhoneNumberBlockFormatter(PhoneNumberFormatter formatter) {
        this.formatter = formatter;
    }

    public static PhoneNumberBlockFormatter ofPattern(String pattern) {
        return ofPattern(pattern, Locale.getDefault());
    }

    public static PhoneNumberBlockFormatter ofPattern(String pattern, Locale locale) {
        return new PhoneNumberBlockFormatter(new PhoneNumberFormatterBuilder().toFormatter(pattern, locale));
    }

    public PhoneNumberBlockFormatter withLocale(Locale locale) {
        PhoneNumberFormatter localizedFormatter = formatter.withLocale(locale);
        return localizedFormatter == formatter ? this : new PhoneNumberBlockFormatter(localizedFormatter);
    }

    public PhoneNumberBlock parse(String text) {
        PhoneNumberFormatterContext context = new PhoneNumberFormatterContext(formatter);
        int pos = 0;
        for (PhoneNumberFormatterBuilder.PhoneNumberPart part : formatter.getPartList()) {
            pos = part.parse(context, text, pos);
            if (pos < 0) {
                throw new IllegalArgumentException("parse exception: " + pos + " " + text);
            }
        }
        CharSequence[] parts = context.parts;
        InternationalPhoneNumber internationalPhoneNumber = formatter.getInternationalPhoneNumber(parts);
        String blockStart = Objects.toString(parts[6], null);
        String blockEnd = Objects.toString(parts[7], null);
        if (parts[8] != null) {
            blockStart = blockStart == null ? "0".repeat(parts[8].length()) : blockStart;
            blockEnd = blockEnd == null ? "9".repeat(parts[8].length()) : blockEnd;
        }
        if (blockStart == null || blockEnd == null) {
            throw new IllegalArgumentException("missing block: " + blockStart + "-" + blockEnd);
        }
        return PhoneNumberBlock.of(internationalPhoneNumber, blockStart, blockEnd);
    }

    public String format(PhoneNumberBlock phoneNumberBlock) {
        PhoneNumberFormatterContext context = new PhoneNumberFormatterContext(formatter, getParts(phoneNumberBlock));
        StringBuilder builder = new StringBuilder();
        for (PhoneNumberFormatterBuilder.PhoneNumberPart part : formatter.getPartList()) {
            part.format(context, builder);
        }
        return builder.toString();
    }

    private String[] getParts(PhoneNumberBlock phoneNumberBlock) {
        InternationalPhoneNumber phoneNumber = phoneNumberBlock.getPhoneNumber();

        String blockStart = phoneNumberBlock.getBlockStart();
        String blockEnd = phoneNumberBlock.getBlockEnd();
        return new String[]{
                phoneNumber.getInternationalDialingPrefix(),
                phoneNumber.getCountryCode(),
                phoneNumber.getNationalAccessCode().orElse(null),
                phoneNumber.getNationalDestinationCode(),
                phoneNumber.getSubscriberNumber(),
                phoneNumber.getExtension().orElse(null),
                blockStart,
                blockEnd
        };
    }
}
