package de.schegge.phone;

import java.util.Optional;

public interface PhoneNumber extends PhoneNumberAccessor {
    Optional<String> getNationalAccessCode();

    default String getCountryCode() {
        throw new UnsupportedOperationException();
    }

    String getNationalDestinationCode();

    String getSubscriberNumber();

    Optional<String> getExtension();

    default String format(PhoneNumberFormatter formatter) {
        return formatter.format(this);
    }
}
