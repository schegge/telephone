package de.schegge.phone.validation;

import java.util.Arrays;

public class NationalDestinationCodeValidator extends AbstractDestinationCodeValidator<NationalDestinationCode> {
    @Override
    public void initialize(NationalDestinationCode constraintAnnotation) {
        nationalDestinationCodes = Arrays.asList(constraintAnnotation.value());
        allowed = constraintAnnotation.allowed();
    }
}
