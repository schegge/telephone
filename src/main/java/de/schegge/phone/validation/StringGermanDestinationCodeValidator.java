package de.schegge.phone.validation;

public class StringGermanDestinationCodeValidator extends AbstractStringDestinationCodeValidator<GermanDestinationCode> {

    public StringGermanDestinationCodeValidator() {
        super(new GermanDestinationCodeValidator());
    }
}
