package de.schegge.phone.validation;

import de.schegge.phone.InternationalPhoneNumber;
import de.schegge.phone.PhoneNumberAccessor;
import de.schegge.phone.PhoneNumberBlock;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

public final class GermanDramaNumbers {

    private GermanDramaNumbers() {
        super();
    }

    private static final Map<String, List<PhoneNumberAccessor>> numbers = Map.ofEntries(
            Map.entry("30", List.of(trunk("30", "23125", "000", "999"))),
            Map.entry("69", List.of(trunk("69", "90009", "000", "999"))),
            Map.entry("40", List.of(trunk("40", "66969", "000", "999"))),
            Map.entry("221", List.of(trunk("221", "4710", "000", "999"))),
            Map.entry("89", List.of(trunk("89", "99998", "000", "999"))),
            Map.entry("152", numbers("152", "28817386", "28895456", "54599371")),
            Map.entry("171", List.of(trunk("171", "39200", "00", "99"))),
            Map.entry("176", List.of(trunk("176", "040690", "00", "99"))),
            Map.entry("172", numbers("172", "9925904", "9968532", "9973185", "9973186", "9980752")),
            Map.entry("174", numbers("174", "9091317", "9464308"))
    );

    public static boolean isValid(InternationalPhoneNumber phoneNumber) {
        return Stream.of(numbers.get(phoneNumber.getNationalDestinationCode())).filter(Objects::nonNull).flatMap(List::stream).anyMatch(x -> x.matches(phoneNumber));
    }

    private static PhoneNumberAccessor trunk(String nationalDestinationCode, String subscriberNumber, String blockStart, String blockEnd) {
        return PhoneNumberBlock.of(InternationalPhoneNumber.of("49", "0", nationalDestinationCode, subscriberNumber), blockStart, blockEnd);
    }

    private static List<PhoneNumberAccessor> numbers(String nationalDestinationCode, String... subscriberNumbers) {
        return Stream.of(subscriberNumbers).map(sn -> InternationalPhoneNumber.of("49", "0", nationalDestinationCode, sn))
                .map(PhoneNumberAccessor.class::cast).toList();
    }
}
