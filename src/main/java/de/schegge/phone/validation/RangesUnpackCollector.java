package de.schegge.phone.validation;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;

public final class RangesUnpackCollector {

    private RangesUnpackCollector() {
        super();
    }

    private static class Accumulator {

        private final List<String> result;
        private Integer last;

        public Accumulator(int first) {
            result = new ArrayList<>();
            result.add(String.valueOf(first));
            last = first;
        }

        public void add(String value) {
            int lastIndex = value.indexOf('+', 1);
            if (lastIndex != -1) {
                last += Integer.parseInt(value.substring(1, lastIndex));
                result.add(String.valueOf(last));
                int count = Integer.parseInt(value.substring(lastIndex));
                for (int i = 0; i < count; i++) {
                    last++;
                    result.add(String.valueOf(last));
                }
            } else {
                last += Integer.parseInt(value.substring(1));
                result.add(String.valueOf(last));
            }
        }

        public List<String> get() {
            return result;
        }
    }

    public static Collector<String, ?, List<String>> unpack(int first) {
        return Collector.of(() -> new Accumulator(first), Accumulator::add, (a, b) -> {
            throw new UnsupportedOperationException("parallel not supported");
        }, Accumulator::get);
    }
}
