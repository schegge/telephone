package de.schegge.phone.validation;

import de.schegge.phone.InternationalPhoneNumber;
import de.schegge.phone.NationalPhoneNumber;
import jakarta.validation.ConstraintValidatorContext;

public class NationalPhoneNumberGermanDramaValidator extends AbstractGermanDramaValidator<NationalPhoneNumber> {
    @Override
    public boolean isValid(NationalPhoneNumber phoneNumber, ConstraintValidatorContext constraintValidatorContext) {
        if (phoneNumber == null) {
            return true;
        }
        return allowed == GermanDramaNumbers.isValid(InternationalPhoneNumber.of("49", phoneNumber));
    }
}
