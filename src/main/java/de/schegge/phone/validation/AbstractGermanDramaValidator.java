package de.schegge.phone.validation;

import de.schegge.phone.PhoneNumber;
import jakarta.validation.ConstraintValidator;

public abstract class AbstractGermanDramaValidator<T extends PhoneNumber> implements ConstraintValidator<GermanDramaNumber, T> {
    protected boolean allowed;

    @Override
    public void initialize(GermanDramaNumber constraintAnnotation) {
        allowed = constraintAnnotation.allowed();
    }
}
