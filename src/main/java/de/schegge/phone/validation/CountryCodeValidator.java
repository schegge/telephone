package de.schegge.phone.validation;

import de.schegge.phone.InternationalPhoneNumber;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class CountryCodeValidator implements ConstraintValidator<CountryCode, InternationalPhoneNumber> {
    private List<String> countryCodes;
    private boolean allowed;

    @Override
    public void initialize(CountryCode constraintAnnotation) {
        countryCodes = Arrays.asList(constraintAnnotation.value());
        allowed = constraintAnnotation.allowed();
    }

    @Override
    public boolean isValid(InternationalPhoneNumber phoneNumber, ConstraintValidatorContext constraintValidatorContext) {
        if (phoneNumber == null) {
            return true;
        }
        return allowed == countryCodes.contains(phoneNumber.getCountryCode());
    }
}
