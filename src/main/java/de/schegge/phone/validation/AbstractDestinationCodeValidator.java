package de.schegge.phone.validation;

import de.schegge.phone.InternationalPhoneNumber;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.lang.annotation.Annotation;
import java.util.List;

public abstract class AbstractDestinationCodeValidator<A extends Annotation> implements ConstraintValidator<A, InternationalPhoneNumber> {
    protected List<String> nationalDestinationCodes;
    protected boolean allowed;

    @Override
    public boolean isValid(InternationalPhoneNumber phoneNumber, ConstraintValidatorContext constraintValidatorContext) {
        if (phoneNumber == null) {
            return true;
        }
        return allowed == nationalDestinationCodes.contains(phoneNumber.getNationalDestinationCode());
    }
}
