package de.schegge.phone.validation;

public class StringNationalDestinationCodeValidator extends AbstractStringDestinationCodeValidator<NationalDestinationCode> {

    public StringNationalDestinationCodeValidator() {
        super(new NationalDestinationCodeValidator());
    }
}
