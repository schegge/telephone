package de.schegge.phone.validation;

import de.schegge.phone.InternationalPhoneNumber;
import jakarta.validation.ConstraintValidatorContext;

public class InternationalPhoneNumberGermanDramaValidator extends AbstractGermanDramaValidator<InternationalPhoneNumber> {
    @Override
    public boolean isValid(InternationalPhoneNumber phoneNumber, ConstraintValidatorContext constraintValidatorContext) {
        if (phoneNumber == null) {
            return true;
        }
        if (!"49".contains(phoneNumber.getCountryCode())) {
            return false;
        }
        return allowed == GermanDramaNumbers.isValid(phoneNumber);
    }
}
