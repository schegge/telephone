package de.schegge.phone.validation;

import de.schegge.phone.InternationalPhoneNumber;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class StringGermanDramaValidator implements ConstraintValidator<GermanDramaNumber, String> {
    private final InternationalPhoneNumberGermanDramaValidator validator = new InternationalPhoneNumberGermanDramaValidator();

    @Override
    public void initialize(GermanDramaNumber constraintAnnotation) {
        validator.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String phoneNumber, ConstraintValidatorContext constraintValidatorContext) {
        return phoneNumber == null || validator.isValid(InternationalPhoneNumber.parse(phoneNumber), constraintValidatorContext);
    }
}