package de.schegge.phone.validation;

import java.util.Arrays;

public class GermanDestinationCodeValidator extends AbstractDestinationCodeValidator<GermanDestinationCode> {

    @Override
    public void initialize(GermanDestinationCode constraintAnnotation) {
        if (constraintAnnotation.value().length > 0) {
            nationalDestinationCodes = Arrays.asList(constraintAnnotation.value());
        } else {
            nationalDestinationCodes = constraintAnnotation.ranges().getAreaCodes();
        }
        allowed = constraintAnnotation.allowed();
    }
}
