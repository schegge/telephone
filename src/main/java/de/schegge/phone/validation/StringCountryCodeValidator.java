package de.schegge.phone.validation;

import de.schegge.phone.InternationalPhoneNumber;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class StringCountryCodeValidator implements ConstraintValidator<CountryCode, String> {
    private final CountryCodeValidator countryCodeValidator = new CountryCodeValidator();

    @Override
    public void initialize(CountryCode constraintAnnotation) {
        countryCodeValidator.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String phoneNumber, ConstraintValidatorContext constraintValidatorContext) {
        if (phoneNumber == null) {
            return true;
        }
        return countryCodeValidator.isValid(InternationalPhoneNumber.parse(phoneNumber), constraintValidatorContext);
    }
}
