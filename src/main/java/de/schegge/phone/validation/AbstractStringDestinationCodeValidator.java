package de.schegge.phone.validation;

import de.schegge.phone.InternationalPhoneNumber;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.lang.annotation.Annotation;

public abstract class AbstractStringDestinationCodeValidator<A extends Annotation> implements ConstraintValidator<A, String> {

    private final AbstractDestinationCodeValidator<A> nationalDestinationCodeValidator;

    protected AbstractStringDestinationCodeValidator(AbstractDestinationCodeValidator<A> nationalDestinationCodeValidator) {
        this.nationalDestinationCodeValidator = nationalDestinationCodeValidator;
    }

    @Override
    public void initialize(A constraintAnnotation) {
        nationalDestinationCodeValidator.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String phoneNumber, ConstraintValidatorContext constraintValidatorContext) {
        if (phoneNumber == null) {
            return true;
        }
        return nationalDestinationCodeValidator.isValid(InternationalPhoneNumber.parse(phoneNumber), constraintValidatorContext);
    }
}
