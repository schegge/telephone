package de.schegge.phone;

import java.util.Locale;
import java.util.Map;

record Localization(String internationalDialingPrefix, String countryCallingCode, String nationalAccessCode) {
    private static final Map<String, Localization> localizations = Map.ofEntries(
            Map.entry("AT", new Localization("00", "43", "0")),
            Map.entry("BE", new Localization("00", "32", "0")),
            Map.entry("BG", new Localization("00", "359", "0")),
            Map.entry("HR", new Localization("00", "385", "0")),
            Map.entry("FI", new Localization("00", "358", "0")),
            Map.entry("FR", new Localization("00", "33", "0")),
            Map.entry("DE", new Localization("00", "49", "0")),
            Map.entry("IE", new Localization("00", "353", "0")),
            Map.entry("LT", new Localization("00", "370", "0")),
            Map.entry("NL", new Localization("00", "31", "0")),
            Map.entry("RO", new Localization("00", "40", "0")),
            Map.entry("SK", new Localization("00", "421", "0")),
            Map.entry("SI", new Localization("00", "386", "0")),
            Map.entry("SE", new Localization("00", "46", "0")),
            Map.entry("UK", new Localization("00", "44", "0")),
            Map.entry("UA", new Localization("00", "380", "0")),
            Map.entry(Locale.US.getCountry(), new Localization("011", "1", "1")),
            Map.entry(Locale.CANADA.getCountry(), new Localization("011", "81", "1")),
            Map.entry(Locale.JAPAN.getCountry(), new Localization("010", "81", "0"))
    );

    static Localization getLocalizations(Locale locale) {
        Localization localization = localizations.get(locale.getCountry());
        if (localization == null) {
            throw new IllegalArgumentException("unsupported country: " + locale);
        }
        return localization;
    }
}
