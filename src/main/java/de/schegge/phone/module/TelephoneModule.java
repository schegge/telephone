package de.schegge.phone.module;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import de.schegge.phone.InternationalPhoneNumber;
import de.schegge.phone.PhoneNumberBlock;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TelephoneModule extends SimpleModule {

    private boolean simpleFormat;
    private boolean defaultFormat;
    private String defaultCountryCallingCode = "49";
    private String defaultInternationalDialingPrefix = "00";
    private String defaultNationalAccessCode = "0";

    public TelephoneModule withSimpleFormat() {
        simpleFormat = true;
        return this;
    }

    public TelephoneModule withDefaultFormat() {
        simpleFormat = false;
        defaultFormat = true;
        return this;
    }

    public TelephoneModule withDefaultCodes(String internationalDialingPrefix, String nationalAccessCode, String countryCallingCode) {
        simpleFormat = false;
        defaultInternationalDialingPrefix = Objects.requireNonNull(internationalDialingPrefix);
        defaultNationalAccessCode = Objects.requireNonNull(nationalAccessCode);
        defaultCountryCallingCode = Objects.requireNonNull(countryCallingCode);
        return this;
    }

    @Override
    public void setupModule(SetupContext context) {
        if (simpleFormat) {
            setUpModules(context, new InternationalPhoneNumberSimpleSerializer(), new InternationalPhoneNumberSimpleDeserializer());
        } else {
            setUpModules(context, new InternationalPhoneNumberSerializer(defaultFormat),
                    new InternationalPhoneNumberDeserializer(defaultInternationalDialingPrefix, defaultNationalAccessCode, defaultCountryCallingCode));
        }
    }

    private void setUpModules(SetupContext context, JsonSerializer<?> ser, JsonDeserializer<?> deser) {
        context.addSerializers(new SimpleSerializers(List.of(ser, new PhoneNumberBlockSimpleSerializer())));
        context.addDeserializers(new SimpleDeserializers(Map.of(InternationalPhoneNumber.class, deser, PhoneNumberBlock.class, new PhoneNumberBlockSimpleDeserializer())));
    }
}
