package de.schegge.phone.module;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import de.schegge.phone.InternationalPhoneNumber;
import java.io.IOException;

final class InternationalPhoneNumberSerializer extends StdSerializer<InternationalPhoneNumber> {

  private final boolean withDefaultFormat;

  InternationalPhoneNumberSerializer(boolean withDefaultFormat) {
    super(InternationalPhoneNumber.class);
    this.withDefaultFormat = withDefaultFormat;
  }

  @Override
  public void serialize(InternationalPhoneNumber value, JsonGenerator gen, SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeStringField("cc", "49");
    gen.writeStringField("ndc", value.getNationalDestinationCode());
    gen.writeStringField("sn", value.getSubscriberNumber());
    if (withDefaultFormat) {
      gen.writeStringField("formatted", value.toString());
    }
    gen.writeEndObject();

  }
}
