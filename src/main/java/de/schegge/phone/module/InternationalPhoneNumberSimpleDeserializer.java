package de.schegge.phone.module;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.util.ClassUtil;
import de.schegge.phone.InternationalPhoneNumber;
import java.io.IOException;

final class InternationalPhoneNumberSimpleDeserializer extends StdDeserializer<InternationalPhoneNumber> {

  InternationalPhoneNumberSimpleDeserializer() {
    super(InternationalPhoneNumber.class);
  }

  @Override
  public InternationalPhoneNumber deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
    try {
      return InternationalPhoneNumber.parse(p.getValueAsString());
    } catch (IllegalArgumentException e) {
      return (InternationalPhoneNumber) ctxt.handleWeirdStringValue(_valueClass, p.getValueAsString(),
          "not a valid representation (error: %s)", ClassUtil.exceptionMessage(e));
    }
  }
}
