package de.schegge.phone.module;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.util.ClassUtil;
import de.schegge.phone.InternationalPhoneNumber;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

final class InternationalPhoneNumberDeserializer extends StdDeserializer<InternationalPhoneNumber> {

  private final String defaultInternationalDialingPrefix;
  private final String defaultNationalAccessCode;
  private final String defaultCountryCallingCode;

  InternationalPhoneNumberDeserializer(String internationalDialingPrefix, String nationalAccessCode,
                                       String countryCallingCode) {
    super(InternationalPhoneNumber.class);
    this.defaultInternationalDialingPrefix = Objects.requireNonNull(internationalDialingPrefix);
    this.defaultNationalAccessCode = Objects.requireNonNull(nationalAccessCode);
    this.defaultCountryCallingCode = Objects.requireNonNull(countryCallingCode);
  }

  @Override
  public InternationalPhoneNumber deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
    try {
      JsonNode node = p.getCodec().readTree(p);
      String cc = Optional.of(node.path("cc")).map(JsonNode::asText).filter(s -> !s.isEmpty())
          .orElse(defaultCountryCallingCode);
      String ndc = node.path("ndc").asText();
      String sn = node.path("sn").asText();
      if (cc.isBlank() || ndc.isBlank() || sn.isBlank()) {
        ctxt.handleWeirdStringValue(_valueClass, p.getValueAsString(),
            "not a valid representation (error: %s)", "cc, ndc or sn is missing");
      }
      return InternationalPhoneNumber.of(defaultInternationalDialingPrefix, cc,defaultNationalAccessCode, ndc, sn);
    } catch (IllegalArgumentException e) {
      return (InternationalPhoneNumber) ctxt.handleWeirdStringValue(_valueClass, p.getValueAsString(),
          "not a valid representation (error: %s)", ClassUtil.exceptionMessage(e));
    }
  }
}
