package de.schegge.phone.module;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import de.schegge.phone.PhoneNumberBlock;

import java.io.IOException;

final class PhoneNumberBlockSimpleSerializer extends StdSerializer<PhoneNumberBlock> {

  PhoneNumberBlockSimpleSerializer() {
    super(PhoneNumberBlock.class);
  }

  @Override
  public void serialize(PhoneNumberBlock value, JsonGenerator gen, SerializerProvider serializers)
      throws IOException {
    gen.writeString(value.toString());
  }
}
