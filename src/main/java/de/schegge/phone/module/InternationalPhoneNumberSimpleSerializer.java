package de.schegge.phone.module;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import de.schegge.phone.InternationalPhoneNumber;
import java.io.IOException;

final class InternationalPhoneNumberSimpleSerializer extends StdSerializer<InternationalPhoneNumber> {

  InternationalPhoneNumberSimpleSerializer() {
    super(InternationalPhoneNumber.class);
  }

  @Override
  public void serialize(InternationalPhoneNumber value, JsonGenerator gen, SerializerProvider serializers)
      throws IOException {
    gen.writeString(value.toString());
  }
}
