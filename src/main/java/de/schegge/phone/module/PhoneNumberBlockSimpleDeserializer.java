package de.schegge.phone.module;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.util.ClassUtil;
import de.schegge.phone.PhoneNumberBlock;

import java.io.IOException;

final class PhoneNumberBlockSimpleDeserializer extends StdDeserializer<PhoneNumberBlock> {

  PhoneNumberBlockSimpleDeserializer() {
    super(PhoneNumberBlock.class);
  }

  @Override
  public PhoneNumberBlock deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
    try {
      return PhoneNumberBlock.parse(p.getValueAsString());
    } catch (IllegalArgumentException e) {
      return (PhoneNumberBlock) ctxt.handleWeirdStringValue(_valueClass, p.getValueAsString(),
          "not a valid representation (error: %s)", ClassUtil.exceptionMessage(e));
    }
  }
}
