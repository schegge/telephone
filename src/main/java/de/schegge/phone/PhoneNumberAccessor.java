package de.schegge.phone;

public interface PhoneNumberAccessor {
    default boolean hasCountryCode() {
        return true;
    }

    boolean matches(PhoneNumber number);
}
