package de.schegge.phone;

import java.util.Optional;

import static java.util.Objects.hash;
import static java.util.Objects.requireNonNull;

public final class InternationalPhoneNumber implements PhoneNumber, PhoneNumberAccessor {

    private final String internationalDialingPrefix;
    private final String countryCode;
    private final NationalPhoneNumber nationalPhoneNumber;

    private InternationalPhoneNumber(String internationalDialingPrefix, String countryCode, NationalPhoneNumber nationalPhoneNumber) {
        this.internationalDialingPrefix = internationalDialingPrefix;
        this.countryCode = countryCode;
        this.nationalPhoneNumber = nationalPhoneNumber;
    }

    public static InternationalPhoneNumber of(InternationalPhoneNumber phoneNumber, NationalPhoneNumber nationalPhoneNumber) {
        return of(phoneNumber.internationalDialingPrefix, phoneNumber.countryCode, nationalPhoneNumber);
    }

    public static InternationalPhoneNumber of(String countryCallingCode, NationalPhoneNumber nationalPhoneNumber) {
        return of("+", countryCallingCode, nationalPhoneNumber);
    }

    public static InternationalPhoneNumber of(String internationalDialingPrefix, String countryCallingCode,
                                              NationalPhoneNumber nationalPhoneNumber) {
        return new InternationalPhoneNumber(requireNonNull(internationalDialingPrefix), requireNonNull(countryCallingCode),
                requireNonNull(nationalPhoneNumber));
    }

    public static InternationalPhoneNumber of(String countryCallingCode, String nationalAccessCode, String nationalDestinationCode, String subscriberNumber) {
        NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.of(nationalAccessCode, nationalDestinationCode, subscriberNumber);
        return of("+", countryCallingCode, nationalPhoneNumber);
    }

    public static InternationalPhoneNumber of(String internationalDialingPrefix, String countryCallingCode,
                                              String nationalAccessCode, String nationalDestinationCode, String subscriberNumber) {
        NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.of(nationalAccessCode, nationalDestinationCode, subscriberNumber);
        return of(internationalDialingPrefix, countryCallingCode, nationalPhoneNumber);
    }

    public static InternationalPhoneNumber of(String internationalDialingPrefix, String countryCallingCode,
                                              String nationalAccessCode, String nationalDestinationCode, String subscriberNumber, String extension) {
        NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.of(nationalAccessCode, nationalDestinationCode, subscriberNumber, extension);
        return of(internationalDialingPrefix, countryCallingCode, nationalPhoneNumber);
    }

    public static InternationalPhoneNumber parse(String text) {
        return PhoneNumberFormatter.INTERNATIONAL_DIN_5008.parse(text);
    }

    public NationalPhoneNumber toNational() {
        return nationalPhoneNumber;
    }

    public String getInternationalDialingPrefix() {
        return internationalDialingPrefix;
    }

    @Override
    public String getCountryCode() {
        return countryCode;
    }

    @Override
    public Optional<String> getNationalAccessCode() {
        return nationalPhoneNumber.getNationalAccessCode();
    }

    @Override
    public String getNationalDestinationCode() {
        return nationalPhoneNumber.getNationalDestinationCode();
    }

    @Override
    public String getSubscriberNumber() {
        return nationalPhoneNumber.getSubscriberNumber();
    }

    @Override
    public Optional<String> getExtension() {
        return nationalPhoneNumber.getExtension();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InternationalPhoneNumber that = (InternationalPhoneNumber) o;
        return internationalDialingPrefix.equals(that.internationalDialingPrefix) && countryCode.equals(that.countryCode) &&
                nationalPhoneNumber.equals(that.nationalPhoneNumber);
    }

    @Override
    public int hashCode() {
        return hash(internationalDialingPrefix, countryCode, nationalPhoneNumber);
    }

    @Override
    public String toString() {
        return PhoneNumberFormatter.INTERNATIONAL_DIN_5008.format(this);
    }

    @Override
    public boolean matches(PhoneNumber number) {
        return equals(number);
    }
}
