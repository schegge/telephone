package de.schegge.phone.freshmarker;

import de.schegge.phone.InternationalPhoneNumber;
import de.schegge.phone.NationalPhoneNumber;
import de.schegge.phone.PhoneNumberBlock;
import ftl.ParseException;
import org.freshmarker.Configuration;
import org.freshmarker.Configuration.TemplateBuilder;
import org.freshmarker.Template;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Locale;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TelephoneInterpolationTest {
    private TemplateBuilder builder;

    @BeforeEach
    public void setUp() {
        Locale.setDefault(Locale.GERMANY);
        builder = new Configuration().builder().withLocale(Locale.GERMANY);
    }

    @ParameterizedTest
    @CsvSource(value = {
            "test: ${internationalNumber};test: +49 173 112233-44",
            "test: ${nationalNumber};test: 0173 112233-44",
            "test: ${numberBlock};test: +49 173 1122 00-99",
            "test: ${internationalNumber?national};test: 0173 112233-44",
            "test: ${internationalNumber?ndc};test: 173",
            "test: ${internationalNumber?sn};test: 112233",
            "test: ${internationalNumber?ext};test: 44",
            "test: ${nationalNumber?ndc};test: 173",
            "test: ${nationalNumber?sn};test: 112233",
            "test: ${nationalNumber?ext};test: 44",
            "test: ${numberBlock?first};test: +49 173 112200",
            "test: ${numberBlock?last};test: +49 173 112299",
            "test: <#list numberBlock?list[0..2] as s with l>${s}<#if !l?is_last>, </#if></#list>;test: +49 173 112200, +49 173 112201, +49 173 112202",
    }, delimiter = ';')
    void interpolationConstant(String templateSource, String expected) throws ParseException {
        Template template = builder.getTemplate("test", templateSource);
        Map<String, Object> dataModel = Map.of(
                "internationalNumber", InternationalPhoneNumber.parse("+49 173 112233-44"),
                "nationalNumber", NationalPhoneNumber.parse("0173 112233-44"),
                "numberBlock", PhoneNumberBlock.parse("+49 173 1122 XX")
        );
        assertEquals(expected, template.process(dataModel));
    }
}