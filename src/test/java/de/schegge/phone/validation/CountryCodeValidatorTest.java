package de.schegge.phone.validation;

import de.schegge.phone.InternationalPhoneNumber;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import jakarta.validation.Validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(ValidatorParameterResolver.class)
class CountryCodeValidatorTest {

  private static class InternationalPhoneNumberDto {

    @CountryCode("49")
    InternationalPhoneNumber germany;

    @CountryCode("33")
    InternationalPhoneNumber france;

    @GermanCountryCode
    InternationalPhoneNumber germany2;

    @CountryCode(value = "49", allowed = false)
    InternationalPhoneNumber notGermany;

    @CountryCode(value = "33", allowed = false)
    InternationalPhoneNumber notFrance;

    @GermanCountryCode(allowed = false)
    InternationalPhoneNumber notGermany2;
  }

  @Test
  void testCorrectValidAllowedPhoneNumber(Validator validator) {
    InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
    dto.germany = InternationalPhoneNumber.of("49", "0","521", "112233");
    dto.germany2 = InternationalPhoneNumber.of( "49","0", "521", "112233");
    dto.france = InternationalPhoneNumber.of( "33", "0","521", "112233");
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testCorrectInvalidAllowedPhoneNumber(Validator validator) {
    InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
    dto.germany = InternationalPhoneNumber.of("33", "0","521", "112233");
    dto.germany2 = InternationalPhoneNumber.of( "33", "0","521", "112233");
    dto.france = InternationalPhoneNumber.of( "49", "0","521", "112233");
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(3, validator.validate(dto).size());
  }

  @Test
  void testCorrectValidDisallowedPhoneNumber(Validator validator) {
    InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
    dto.notGermany = InternationalPhoneNumber.of("33", "0","521", "112233");
    dto.notGermany2 = InternationalPhoneNumber.of( "33", "0","521", "112233");
    dto.notFrance = InternationalPhoneNumber.of( "49", "0","521", "112233");
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testCorrectInvalidDisallowedPhoneNumber(Validator validator) {
    InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
    dto.notGermany = InternationalPhoneNumber.of( "49","0", "521", "112233");
    dto.notGermany2 = InternationalPhoneNumber.of( "49", "0","521", "112233");
    dto.notFrance = InternationalPhoneNumber.of( "33", "0","521", "112233");
    assertFalse(validator.validate(dto).isEmpty());
    assertEquals(3, validator.validate(dto).size());
  }

  @Test
  void testNull(Validator validator) {
    assertTrue(validator.validate(new InternationalPhoneNumberDto()).isEmpty());
  }
}

