package de.schegge.phone.validation;

import de.schegge.phone.InternationalPhoneNumber;
import de.schegge.phone.PhoneNumberBlock;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertTrue;

class GermanDramaNumbersTest {
    @ParameterizedTest
    @CsvSource({"+49 30 23125 000-999", "+49 69 90009 000-999", "+49 40 66969 000-999", "+49 221 4710 000-999", "+49 89 99998 000-999",
            "+49 171 39200 00-99", "+49 176 040690 00-99"})
    void phoneNumberBlock(String number) {
        assertTrue(PhoneNumberBlock.parse(number).phoneNumbers().allMatch(GermanDramaNumbers::isValid));
    }

    @ParameterizedTest
    @CsvSource({"+49 152 28817386", "+49 152 28895456", "+49 152 54599371", "+49 172 9925904", "+49 172 9968532", "+49 172 9973185",
            "+49 172 9973186", "+49 172 9980752", "+49 174 9091317", "+49 174 9464308"})
    void internationalPhoneNumber(String number) {
        assertTrue(GermanDramaNumbers.isValid(InternationalPhoneNumber.parse(number)));
    }
}
