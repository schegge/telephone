package de.schegge.phone.validation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import jakarta.validation.Validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(ValidatorParameterResolver.class)
class StringCountryCodeValidatorTest {

    private static class InternationalPhoneNumberDto {

        @CountryCode("49")
        String germany;

        @CountryCode("33")
        String france;

        @GermanCountryCode
        String germany2;


        @CountryCode(value = "49", allowed = false)
        String notGermany;

        @CountryCode(value = "33", allowed = false)
        String notFrance;

        @GermanCountryCode(allowed = false)
        String gnotGermany2;
    }

    @Test
    void testCorrectValidAllowedPhoneNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.germany = "+49 521 112233";
        dto.germany2 = "+49 521 112233";
        dto.france = "+33 521 112233";
        assertTrue(validator.validate(dto).isEmpty());
    }

    @Test
    void testCorrectInvalidAllowedPhoneNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.germany = "+33 521 112233";
        dto.germany2 = "+33 521 112233";
        dto.france = "+49 521 112233";
        assertFalse(validator.validate(dto).isEmpty());
        assertEquals(3, validator.validate(dto).size());
    }

    @Test
    void testCorrectValidDisallowedPhoneNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.notGermany = "+33 521 112233";
        dto.gnotGermany2 = "+33 521 112233";
        dto.notFrance = "+49 521 112233";
        assertTrue(validator.validate(dto).isEmpty());
    }

    @Test
    void testCorrectInvalidDisallowedPhoneNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.notGermany = "+49 521 112233";
        dto.gnotGermany2 = "+49 521 112233";
        dto.notFrance = "+33 521 112233";
        assertFalse(validator.validate(dto).isEmpty());
        assertEquals(3, validator.validate(dto).size());
    }

    @Test
    void testNull(Validator validator) {
        assertTrue(validator.validate(new InternationalPhoneNumberDto()).isEmpty());
    }
}

