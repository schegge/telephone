package de.schegge.phone.validation;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class RangesUnpackCollectorTest {

    @Test
    void singleElement() {
        assertEquals(List.of("42"), Stream.<String>empty().collect(RangesUnpackCollector.unpack(42)));
    }

    @Test
    void ranges() {
        assertEquals(List.of("30", "40","69","89", "201","202","203"), Stream.of("+10","+29","+20","+112+2").collect(RangesUnpackCollector.unpack(30)));
    }
}