package de.schegge.phone.validation;

import de.schegge.phone.InternationalPhoneNumber;
import de.schegge.phone.NationalPhoneNumber;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(ValidatorParameterResolver.class)
class GermanDramaNumberValidatorTest {

  private static class DramaNumberDto {

    @GermanDramaNumber
    InternationalPhoneNumber internationalPhoneNumber;

    @GermanDramaNumber
    NationalPhoneNumber nationalPhoneNumber;

    @GermanDramaNumber
    String string;
  }

  @Test
  void testCorrectValidDramaNumber(Validator validator) {
    DramaNumberDto dto = new DramaNumberDto();
    dto.internationalPhoneNumber = InternationalPhoneNumber.of("49", "0","30", "23125666");
    dto.nationalPhoneNumber = NationalPhoneNumber.of("0", "30",  "23125666");
    dto.string = "+49 30 23125666";
    assertTrue(validator.validate(dto).isEmpty());
  }

  @Test
  void testCorrectInvalidDramaNumber(Validator validator) {
    DramaNumberDto dto = new DramaNumberDto();
    dto.internationalPhoneNumber = InternationalPhoneNumber.of("49", "0","30", "112233");
    dto.nationalPhoneNumber = NationalPhoneNumber.of("0", "30",  "112233");
    dto.string = "+49 30 112233";
    assertFalse(validator.validate(dto).isEmpty());
  }

  @Test
  void testNull(Validator validator) {
    assertTrue(validator.validate(new DramaNumberDto()).isEmpty());
  }
}

