package de.schegge.phone.validation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import jakarta.validation.Validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(ValidatorParameterResolver.class)
class StringNationalDestinationValidatorTest {

    private static class InternationalPhoneNumberDto {

        @NationalDestinationCode("521")
        String bielefeld;

        @NationalDestinationCode("5261")
        String lemgo;

        @GermanMobileDestinationCode
        String mobile;

        @NationalDestinationCode(value = "521", allowed = false)
        String notBielefeld;

        @NationalDestinationCode(value = "5261", allowed = false)
        String notLemgo;
    }


    @Test
    void testValidAllowedPhoneNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.bielefeld = "+49 521 112233";
        dto.lemgo = "+49 5261 112233";
        assertTrue(validator.validate(dto).isEmpty());
    }

    @Test
    void testInvalidAllowedPhoneNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.bielefeld = "+49 5261 112233";
        dto.lemgo = "+49 521 112233";
        assertFalse(validator.validate(dto).isEmpty());
        assertEquals(2, validator.validate(dto).size());
    }

    @Test
    void testValidDisallowedPhoneNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.notBielefeld = "+49 5261 112233";
        dto.notLemgo = "+49 521 112233";
        assertTrue(validator.validate(dto).isEmpty());
    }

    @Test
    void tesInvalidDisallowedPhoneNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.notBielefeld = "+49 521 112233";
        dto.notLemgo = "+49 5261 112233";
        assertFalse(validator.validate(dto).isEmpty());
        assertEquals(2, validator.validate(dto).size());
    }

    @Test
    void testValidAllowedGermanMobileNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.mobile = "+49 172 112233";
        assertTrue(validator.validate(dto).isEmpty());
    }

    @Test
    void testInvalidAllowedGermanMobileNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.mobile = "+49 521 112233";
        assertFalse(validator.validate(dto).isEmpty());
        assertEquals(1, validator.validate(dto).size());
    }

    @Test
    void testNull(Validator validator) {
        assertTrue(validator.validate(new InternationalPhoneNumberDto()).isEmpty());
    }
}

