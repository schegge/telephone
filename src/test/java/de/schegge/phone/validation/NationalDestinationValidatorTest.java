package de.schegge.phone.validation;

import de.schegge.phone.InternationalPhoneNumber;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(ValidatorParameterResolver.class)
class NationalDestinationValidatorTest {

    private static class InternationalPhoneNumberDto {

        @NationalDestinationCode("521")
        InternationalPhoneNumber bielefeld;

        @NationalDestinationCode("5261")
        InternationalPhoneNumber lemgo;

        @GermanMobileDestinationCode
        InternationalPhoneNumber mobile;

        @GermanAreaCode
        InternationalPhoneNumber area;

        @GermanAreaCode
        InternationalPhoneNumber germany;

        @NationalDestinationCode(value = "521", allowed = false)
        InternationalPhoneNumber notBielefeld;

        @NationalDestinationCode(value = "5261", allowed = false)
        InternationalPhoneNumber notLemgo;
    }


    @Test
    void testValidAllowedPhoneNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.bielefeld = InternationalPhoneNumber.of("49", "0", "521", "112233");
        dto.lemgo = InternationalPhoneNumber.of("49", "0", "5261", "112233");
        assertTrue(validator.validate(dto).isEmpty());
    }

    @Test
    void testInvalidAllowedPhoneNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.bielefeld = InternationalPhoneNumber.of("49", "0", "5261", "112233");
        dto.lemgo = InternationalPhoneNumber.of("49", "0", "521", "112233");
        assertFalse(validator.validate(dto).isEmpty());
        assertEquals(2, validator.validate(dto).size());
    }

    @Test
    void testValidDisallowedPhoneNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.notBielefeld = InternationalPhoneNumber.of("49", "0", "5261", "112233");
        dto.notLemgo = InternationalPhoneNumber.of("49", "0", "521", "112233");
        assertTrue(validator.validate(dto).isEmpty());
    }

    @Test
    void tesInvalidDisallowedPhoneNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.notBielefeld = InternationalPhoneNumber.of("49", "0", "521", "112233");
        dto.notLemgo = InternationalPhoneNumber.of("49", "0", "5261", "112233");
        assertFalse(validator.validate(dto).isEmpty());
        assertEquals(2, validator.validate(dto).size());
    }

    @Test
    void testValidAllowedGermanMobileNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.mobile = InternationalPhoneNumber.of("49", "0", "172", "9980752");
        dto.germany = dto.area;
        assertTrue(validator.validate(dto).isEmpty());
    }

    @Test
    void testInvalidAllowedGermanMobileNumber(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.mobile = InternationalPhoneNumber.of("49", "0", "30", "23125666");
        dto.germany = InternationalPhoneNumber.of("1", "00", "172", "112233");
        assertFalse(validator.validate(dto).isEmpty());
        assertEquals(3, validator.validate(dto).size());
    }

    @Test
    void testValidAllowedGermanAreaCode(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.area = InternationalPhoneNumber.of("49", "0", "30", "23125666");
        dto.germany = dto.area;
        assertTrue(validator.validate(dto).isEmpty());
    }

    @Test
    void testInvalidAllowedGermanAreaCode(Validator validator) {
        InternationalPhoneNumberDto dto = new InternationalPhoneNumberDto();
        dto.area = InternationalPhoneNumber.of("49", "0", "172", "112233");
        dto.germany = InternationalPhoneNumber.of("1", "00", "172", "112233");
        assertFalse(validator.validate(dto).isEmpty());
        assertEquals(3, validator.validate(dto).size());
    }

    @Test
    void testNull(Validator validator) {
        assertTrue(validator.validate(new InternationalPhoneNumberDto()).isEmpty());
    }
}

