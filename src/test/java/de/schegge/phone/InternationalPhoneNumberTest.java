package de.schegge.phone;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InternationalPhoneNumberTest {

  @Test
  void format() {
    InternationalPhoneNumber internationalPhoneNumber = InternationalPhoneNumber.of("00", "49", "0", "30", "23125666");
    assertEquals("+49 30 23125666", internationalPhoneNumber.format(PhoneNumberFormatter.INTERNATIONAL_DIN_5008));
    assertEquals("030 23125666", internationalPhoneNumber.format(PhoneNumberFormatter.DIN_5008));
    assertEquals("+49 30 23125666", internationalPhoneNumber.toString());
  }

  @Test
  void parse() {
    NationalPhoneNumber withoutExtension = NationalPhoneNumber.of("0", "30", "23125666");
    assertEquals(InternationalPhoneNumber.of("+", "49", withoutExtension), InternationalPhoneNumber.parse("+49 30 23125666"));

    NationalPhoneNumber withExtension = NationalPhoneNumber.of("0", "30", "23125", "666");
    assertEquals(InternationalPhoneNumber.of("+", "49", withExtension), InternationalPhoneNumber.parse("+49 30 23125-666"));
  }

  @Test
  void parseIllegal() {
    assertThrows(IllegalArgumentException.class, () -> InternationalPhoneNumber.parse("030 23125-666"));
    assertThrows(IllegalArgumentException.class, () -> InternationalPhoneNumber.parse("030 23125666"));
  }

  @Test
  void matches() {
    NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.of("0", "30", "23125666");
    InternationalPhoneNumber internationalPhoneNumber1 = InternationalPhoneNumber.of("00", "49", "0", "30", "23125666");
    InternationalPhoneNumber internationalPhoneNumber2 = InternationalPhoneNumber.of("00", "49", "0", "30", "23125666");
    assertTrue(internationalPhoneNumber1.matches(internationalPhoneNumber2));
    assertFalse(internationalPhoneNumber1.matches(nationalPhoneNumber));
  }
}