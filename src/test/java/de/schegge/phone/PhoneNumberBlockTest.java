package de.schegge.phone;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PhoneNumberBlockTest {

    @Test
    void format() {
        PhoneNumberBlock phoneNumberBlock = PhoneNumberBlock.of(InternationalPhoneNumber.of("00", "49", "0", "5223", "14862"), "00", "99");
        assertEquals("+49 5223 14862 00-99", phoneNumberBlock.format(PhoneNumberBlockFormatter.INTERNATIONAL_BLOCK));
        assertEquals("05223 14862 00-99", phoneNumberBlock.format(PhoneNumberBlockFormatter.NATIONAL_BLOCK));
        assertEquals("+49 5223 14862 00-99", phoneNumberBlock.toString());
        assertEquals("+49 5223 14862 [00-99]", phoneNumberBlock.format(PhoneNumberBlockFormatter.ofPattern("+ccc nnn sss[-eee] ZZZ")));
        assertEquals("+49 5223 14862 00-99", phoneNumberBlock.format(PhoneNumberBlockFormatter.ofPattern("+ccc nnn sss[-eee] zzz")));
        assertEquals("+49 5223 14862 [00-99]", phoneNumberBlock.format(PhoneNumberBlockFormatter.ofPattern("+ccc nnn sss[-eee] BBB")));
        assertEquals("+49 5223 14862 00-99", phoneNumberBlock.format(PhoneNumberBlockFormatter.ofPattern("+ccc nnn sss[-eee] bbb")));
        assertEquals("+49 5223 14862 [XX]", phoneNumberBlock.format(PhoneNumberBlockFormatter.ofPattern("+ccc nnn sss[-eee] XXX")));
        assertEquals("+49 5223 14862 XX", phoneNumberBlock.format(PhoneNumberBlockFormatter.ofPattern("+ccc nnn sss[-eee] xxx")));
    }

    @ParameterizedTest
    @CsvSource({
            "+ccc nnn sss ZZ,+49 5223 14862 [00-99]",
            "+ccc nnn sss zz,+49 5223 14862 00-99",
            "+ccc nnn sss ZZ,+49 5223 14862 [11-66]",
            "+ccc nnn sss zz,+49 5223 14862 11-66",
            "+ccc nnn sss BB,+49 5223 14862 [00-99]",
            "+ccc nnn sss bb,+49 5223 14862 00-99",
            "+ccc nnn sss ZZ,+49 5223 14862 [11-66]",
            "+ccc nnn sss zz,+49 5223 14862 11-66",
            "+ccc nnn sss XX,+49 5223 14862 [XX]",
            "+ccc nnn sss xx,+49 5223 14862 XX",
    })
    void parse(String pattern, String input) {
        PhoneNumberBlockFormatter formatter = PhoneNumberBlockFormatter.ofPattern(pattern);
        assertEquals(input, formatter.parse(input).format(formatter));
    }

    @ParameterizedTest
    @CsvSource({
            "+ccc nnn sss ZZ,+49 5223 14862 [XX], +49 5223 14862 [00-99]",
            "+ccc nnn sss zz,+49 5223 14862 XX,+49 5223 14862 00-99",
    })
    void parse(String pattern, String input, String output) {
        PhoneNumberBlockFormatter formatter = PhoneNumberBlockFormatter.ofPattern(pattern);
        assertEquals(output, formatter.parse(input).format(formatter));
    }

    @Test
    void parseIllegal() {
        assertThrows(IllegalArgumentException.class, () -> PhoneNumberBlock.parse("+49 5223 14862 [YYY]"));
        assertThrows(IllegalArgumentException.class, () -> PhoneNumberBlock.parse("+49 5223 14862 1234-345"));
        assertThrows(IllegalArgumentException.class, () -> PhoneNumberBlock.parse("+49 5223 14862 1234-abc"));
        PhoneNumberFormatter phoneNumberFormatter = PhoneNumberFormatter.ofPattern("+ccc nnn sss zz");
        assertThrows(IllegalArgumentException.class, () -> phoneNumberFormatter.parse("+49 5223 14862 110-666"));
    }

    @ParameterizedTest
    @CsvSource({
            "+ccc nnn sss zz,+49 5223 14862 110-666,parse exception at position 16: +49 5223 14862 110-666",
            "+ccc nnn sss zz,+49 5223 14862 10-666,parse exception at position 16: +49 5223 14862 10-666",
            "+ccc nnn sss zz,+49 5223 14862 XXX,parse exception at position 16: +49 5223 14862 XXX"
    })
    void parseIllegal(String pattern, String input, String message) {
        PhoneNumberFormatter phoneNumberFormatter = PhoneNumberFormatter.ofPattern(pattern);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> phoneNumberFormatter.parse(input));
        assertEquals(message, exception.getMessage());
    }

    @Test
    void stream() {
        assertEquals(1000, PhoneNumberBlock.parse("+49 30 23125 000-999").phoneNumbers().count());
    }

    @Test
    void blockSize() {
        assertEquals(1000, PhoneNumberBlock.parse("+49 30 23125 000-999").getBlockSize());
        assertEquals(100, PhoneNumberBlock.parse("+49 30 231250 00-99").getBlockSize());
    }

    @Test
    void matches() {
        NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.of("0", "30", "23125666");
        InternationalPhoneNumber internationalPhoneNumber1 = InternationalPhoneNumber.of("00", "49", "0", "30", "23125666");
        InternationalPhoneNumber internationalPhoneNumber2 = InternationalPhoneNumber.of("00", "49", "0", "30", "23125701");
        PhoneNumberBlock phoneNumberBlock = PhoneNumberBlock.of(InternationalPhoneNumber.of("00", "49", "0", "30", "23125"), "000", "700");
        assertTrue(phoneNumberBlock.matches(internationalPhoneNumber1));
        assertFalse(phoneNumberBlock.matches(internationalPhoneNumber2));
        assertFalse(phoneNumberBlock.matches(nationalPhoneNumber));
        assertFalse(phoneNumberBlock.matches(null));
    }

    @ParameterizedTest
    @CsvSource({
            "0,99,invalid block: 0-99",
            "A,B,invalid block: A-B",
            "99,00,invalid block: 99-00"
    })
    void createIllegal(String blockBegin, String blockEnd, String message) {
        InternationalPhoneNumber internationalPhoneNumber = InternationalPhoneNumber.of("00", "49", "0", "30", "23125666");
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> PhoneNumberBlock.of(internationalPhoneNumber, blockBegin, blockEnd));
        assertEquals(message, exception.getMessage());
    }

    @Test
    void equality() {
        PhoneNumberBlock numberBlock1 = PhoneNumberBlock.parse("+49 30 23125 000-999");
        PhoneNumberBlock numberBlock2 = PhoneNumberBlock.parse("+49 30 23125 000-999");
        PhoneNumberBlock numberBlock3 = PhoneNumberBlock.parse("+49 30 23125 00-99");
        assertEquals(numberBlock1, numberBlock2);
        assertNotEquals(numberBlock1, numberBlock3);
        assertEquals(numberBlock1.hashCode(), numberBlock2.hashCode());
    }

}