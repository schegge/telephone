package de.schegge.phone;

import org.junit.jupiter.api.Test;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NationalPhoneNumberTest {
  @Test
  void format() {
    NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.of("0", "5223", "14862");
    assertEquals("+null 5223 14862", nationalPhoneNumber.format(PhoneNumberFormatter.INTERNATIONAL_DIN_5008));
    assertEquals("05223 14862", nationalPhoneNumber.format(PhoneNumberFormatter.DIN_5008));
    assertEquals("05223 14862", nationalPhoneNumber.toString());
  }

  @Test
  void parse() {
    NationalPhoneNumber withoutExtension = NationalPhoneNumber.of("0", "5223", "14862");
    assertEquals(withoutExtension, NationalPhoneNumber.parse("05223 14862"));

    NationalPhoneNumber withExtension = NationalPhoneNumber.of("0", "5223", "14862", "62");
    assertEquals(withExtension, NationalPhoneNumber.parse("05223 14862-62"));
    assertEquals("05223 14862-62", NationalPhoneNumber.parse("05223 14862-62").format(PhoneNumberFormatter.DIN_5008));
  }

  @Test
  void parseIllegal() {
    assertThrows(IllegalArgumentException.class, () -> NationalPhoneNumber.parse("+49 30 23125666"));
    assertThrows(IllegalArgumentException.class, () -> NationalPhoneNumber.parse("+49 30 23125666"));
  }

  @Test
  void toInternationalWithLocale() {
    NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.of("0", "30", "23125666");
    InternationalPhoneNumber internationalPhoneNumber = nationalPhoneNumber.toInternational(Locale.GERMANY);
    assertEquals("00", internationalPhoneNumber.getInternationalDialingPrefix());
    assertEquals("49", internationalPhoneNumber.getCountryCode());
  }

  @Test
  void matches() {
    InternationalPhoneNumber internationalPhoneNumber = InternationalPhoneNumber.of("00", "49", "0", "30", "23125666");
    NationalPhoneNumber nationalPhoneNumber1 = NationalPhoneNumber.of("0", "30", "23125666");
    NationalPhoneNumber nationalPhoneNumber2 = NationalPhoneNumber.of("0", "30", "23125666");
    assertTrue(nationalPhoneNumber1.matches(nationalPhoneNumber2));
    assertFalse(nationalPhoneNumber1.matches(internationalPhoneNumber));
  }

  @Test
  void getCountryCode() {
    NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.parse("030 23125666");
    assertThrows(UnsupportedOperationException.class, nationalPhoneNumber::getCountryCode);
  }

  @Test
  void from() {
    NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.parse("030 23125666");
    InternationalPhoneNumber internationalPhoneNumber = nationalPhoneNumber.toInternational(Locale.GERMANY);
    assertEquals(nationalPhoneNumber, NationalPhoneNumber.from(nationalPhoneNumber));
    assertEquals(nationalPhoneNumber, NationalPhoneNumber.from(internationalPhoneNumber));
  }

  @Test
  void fromWithExtension() {
    NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.parse("030 23125666-111");
    InternationalPhoneNumber internationalPhoneNumber = nationalPhoneNumber.toInternational(Locale.GERMANY);
    assertEquals(nationalPhoneNumber, NationalPhoneNumber.from(nationalPhoneNumber));
    assertEquals(nationalPhoneNumber, NationalPhoneNumber.from(internationalPhoneNumber));
  }
}