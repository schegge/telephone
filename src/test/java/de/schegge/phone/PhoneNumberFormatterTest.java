package de.schegge.phone;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Locale;
import java.util.Optional;

import static de.schegge.phone.PhoneNumberFormatter.DIN_5008;
import static de.schegge.phone.PhoneNumberFormatter.E_123;
import static de.schegge.phone.PhoneNumberFormatter.INTERNATIONAL_DIN_5008;
import static de.schegge.phone.PhoneNumberFormatter.INTERNATIONAL_E_123;
import static de.schegge.phone.PhoneNumberFormatter.PREFIXED_DIN_5008;
import static de.schegge.phone.PhoneNumberFormatter.PREFIXED_E_123;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PhoneNumberFormatterTest {

    @BeforeEach
    void setUp() {
        Locale.setDefault(Locale.GERMANY);
    }

    @ParameterizedTest
    @CsvSource({
            "annn sss[-eee",
            "annn sss-eee]",
            "annn sss[-ggg]",
            "annn sss[#eee]",
    })
    void invalidOfPattern(String pattern) {
        assertThrows(IllegalArgumentException.class, () -> PhoneNumberFormatter.ofPattern(pattern, Locale.GERMANY));
    }

    @Test
    void format() {
        NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.of("0", "5223", "112233");
        assertEquals("05223 112233", DIN_5008.format(nationalPhoneNumber));
        assertEquals("(05223) 112233", E_123.format(nationalPhoneNumber));

        NationalPhoneNumber nationalPhoneNumberWithExtension = NationalPhoneNumber.of("0", "5223", "112233", "00");
        assertEquals("05223 112233-00", DIN_5008.format(nationalPhoneNumberWithExtension));
        assertEquals("(05223) 112233-00", E_123.format(nationalPhoneNumberWithExtension));

        InternationalPhoneNumber internationalPhoneNumber = InternationalPhoneNumber.of("49", nationalPhoneNumber);
        assertEquals("+49 5223 112233", PREFIXED_DIN_5008.format(internationalPhoneNumber));
        assertEquals("+49 (5223) 112233", PREFIXED_E_123.format(internationalPhoneNumber));
        assertEquals("+49 5223 112233", INTERNATIONAL_DIN_5008.format(internationalPhoneNumber));
        assertEquals("+49 (5223) 112233", INTERNATIONAL_E_123.format(internationalPhoneNumber));
    }

    @Test
    void formatUnknown() {
        assertThrows(IllegalArgumentException.class, () -> INTERNATIONAL_DIN_5008.format(new PhoneNumber() {
            @Override
            public Optional<String> getNationalAccessCode() {
                return Optional.empty();
            }

            @Override
            public String getNationalDestinationCode() {
                return "";
            }

            @Override
            public String getSubscriberNumber() {
                return "";
            }

            @Override
            public Optional<String> getExtension() {
                return Optional.empty();
            }

            @Override
            public boolean matches(PhoneNumber number) {
                return false;
            }
        }));
    }

    @Test
    void parse() {
        NationalPhoneNumber nationalPhoneNumber = NationalPhoneNumber.of("0", "5223", "112233");
        assertEquals(nationalPhoneNumber, DIN_5008.parse("05223 112233").toNational());
        assertEquals(nationalPhoneNumber, E_123.parse("(05223) 112233").toNational());

        InternationalPhoneNumber internationalPhoneNumber = InternationalPhoneNumber.of("00", "49", "0", "5223", "112233");
        assertEquals(internationalPhoneNumber, PREFIXED_DIN_5008.parse("0049 5223 112233"));
        InternationalPhoneNumber internationalPhoneNumberWithPlus = InternationalPhoneNumber.of("+", "49", "0", "5223", "112233");
        assertEquals(internationalPhoneNumberWithPlus, PREFIXED_DIN_5008.parse("+49 5223 112233"));
        assertEquals(internationalPhoneNumber, PhoneNumberFormatter.ofPattern("iiiccc nnn sss[-eee]").parse("0049 5223 112233"));

        NationalPhoneNumber nationalPhoneNumberWithExtension = NationalPhoneNumber.of("0", "5223", "112233", "00");
        assertEquals(nationalPhoneNumberWithExtension, DIN_5008.parse("05223 112233-00").toNational());

        InternationalPhoneNumber internationalPhoneNumberWithExtension = InternationalPhoneNumber.of("00", "49", "0", "5223", "112233", "00");
        assertEquals(internationalPhoneNumberWithExtension, PhoneNumberFormatter.ofPattern("iiiccc nnn sss[-eee]").parse("0049 5223 112233-00"));
    }

    @Test
    void withLocale() {
        PhoneNumberFormatter phoneNumberFormatter = DIN_5008.withLocale(Locale.CANADA);
        assertSame(phoneNumberFormatter, phoneNumberFormatter.withLocale(Locale.CANADA));
        PhoneNumber phoneNumber = phoneNumberFormatter.parse("15223 112233");
        assertEquals("15223 112233", phoneNumberFormatter.format(phoneNumber));
    }
}