package de.schegge.phone.module;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import de.schegge.phone.InternationalPhoneNumber;
import de.schegge.phone.PhoneNumberBlock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SimpleTelephoneModuleTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper().registerModule(new TelephoneModule().withSimpleFormat());
    }

    @Test
    void deserializeInternationalPhoneNumber() throws JsonProcessingException {
        InternationalPhoneNumber internationalPhoneNumber = objectMapper.readValue("\"+49 521 112233\"",
                InternationalPhoneNumber.class);
        assertAll(
                () -> assertEquals("49", internationalPhoneNumber.getCountryCode()),
                () -> assertEquals("521", internationalPhoneNumber.getNationalDestinationCode()),
                () -> assertEquals("112233", internationalPhoneNumber.getSubscriberNumber()));
    }

    @Test
    void deserializePhoneNumberBlock() throws JsonProcessingException {
        PhoneNumberBlock phoneNumberBlock = objectMapper.readValue("\"+49 521 1122 XX\"",
                PhoneNumberBlock.class);
        assertAll(
                () -> assertEquals("49", phoneNumberBlock.getPhoneNumber().getCountryCode()),
                () -> assertEquals("521", phoneNumberBlock.getPhoneNumber().getNationalDestinationCode()),
                () -> assertEquals("1122", phoneNumberBlock.getPhoneNumber().getSubscriberNumber()),
                () -> assertEquals("00", phoneNumberBlock.getBlockStart()),
                () -> assertEquals("99", phoneNumberBlock.getBlockEnd())
        );
    }

    @Test
    void deserializeInvalidInternationalPhoneNumber() {
        InvalidFormatException invalidFormatException = assertThrows(InvalidFormatException.class,
                () -> objectMapper.readValue("\"+49 521\"", InternationalPhoneNumber.class));
        assertEquals(
                "Cannot deserialize value of type `de.schegge.phone.InternationalPhoneNumber` from String \"+49 521\": not a valid representation (error: parse exception at position 8: +49 521)\n" +
                        " at [Source: REDACTED (`StreamReadFeature.INCLUDE_SOURCE_IN_LOCATION` disabled); line: 1, column: 1]",
                invalidFormatException.getMessage());
    }

    @Test
    void serializeInternationalPhoneNumber() throws JsonProcessingException {
        InternationalPhoneNumber internationalPhoneNumber = InternationalPhoneNumber.of("49", "0", "521", "112233");
        assertEquals("\"+49 521 112233\"", objectMapper.writeValueAsString(internationalPhoneNumber));
    }

    @Test
    void serializePhoneNumberBlock() throws JsonProcessingException {
        PhoneNumberBlock phoneNumberBlock = PhoneNumberBlock.of(InternationalPhoneNumber.of("49", "0", "521", "1122"), "11", "22");
        assertEquals("\"+49 521 1122 11-22\"", objectMapper.writeValueAsString(phoneNumberBlock));
    }
}