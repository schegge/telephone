package de.schegge.phone.module;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import de.schegge.phone.InternationalPhoneNumber;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TelephoneModuleTest {
    private record Member(InternationalPhoneNumber phoneNumber) {

    }

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper().registerModule(new TelephoneModule());
    }

    @Test
    void deserialize() throws JsonProcessingException {
        InternationalPhoneNumber internationalPhoneNumber = objectMapper.readValue(
                "{ \"cc\": \"49\", \"ndc\": \"521\", \"sn\": \"112233\"}", InternationalPhoneNumber.class);
        assertAll(
                () -> assertEquals("49", internationalPhoneNumber.getCountryCode()),
                () -> assertEquals("521", internationalPhoneNumber.getNationalDestinationCode()),
                () -> assertEquals("112233", internationalPhoneNumber.getSubscriberNumber()));

        Member member = objectMapper.readValue(
                "{ \"phoneNumber\": { \"cc\": \"49\", \"ndc\": \"521\", \"sn\": \"112233\"}}", Member.class);
        assertAll(
                () -> assertEquals("49", member.phoneNumber().getCountryCode()),
                () -> assertEquals("521", member.phoneNumber().getNationalDestinationCode()),
                () -> assertEquals("112233", member.phoneNumber().getSubscriberNumber()));
    }

    @Test
    void deserializeInvalid() {
        InvalidFormatException invalidFormatException = assertThrows(InvalidFormatException.class,
                () -> objectMapper.readValue("{ \"cc\": \"49\", \"sn\": \"112233\"}", InternationalPhoneNumber.class));
        assertEquals(
                "Cannot deserialize value of type `de.schegge.phone.InternationalPhoneNumber` from String [N/A]: not a valid representation (error: cc, ndc or sn is missing)\n" +
                        " at [Source: REDACTED (`StreamReadFeature.INCLUDE_SOURCE_IN_LOCATION` disabled); line: 1, column: 29]",
                invalidFormatException.getMessage());
    }


    @Test
    void deserializeWithDefaults() throws JsonProcessingException {
        objectMapper = new ObjectMapper().registerModule(new TelephoneModule().withDefaultCodes("00", "0", "49"));
        InternationalPhoneNumber internationalPhoneNumber = objectMapper.readValue(
                "{ \"ndc\": \"521\", \"sn\": \"112233\"}", InternationalPhoneNumber.class);
        assertAll(
                () -> assertEquals("49", internationalPhoneNumber.getCountryCode()),
                () -> assertEquals("521", internationalPhoneNumber.getNationalDestinationCode()),
                () -> assertEquals("112233", internationalPhoneNumber.getSubscriberNumber()));
    }

    @Test
    void serialize() throws JsonProcessingException {
        InternationalPhoneNumber internationalPhoneNumber = InternationalPhoneNumber.of("49", "0", "521", "112233");
        assertEquals("{\"cc\":\"49\",\"ndc\":\"521\",\"sn\":\"112233\"}",
                objectMapper.writeValueAsString(internationalPhoneNumber));
    }

    @Test
    void serializeWithDefaultFormat() throws JsonProcessingException {
        objectMapper = new ObjectMapper().registerModule(new TelephoneModule().withDefaultFormat());
        InternationalPhoneNumber internationalPhoneNumber = InternationalPhoneNumber.of("49", "0", "521", "112233");
        assertEquals("{\"cc\":\"49\",\"ndc\":\"521\",\"sn\":\"112233\",\"formatted\":\"+49 521 112233\"}",
                objectMapper.writeValueAsString(internationalPhoneNumber));
    }
}